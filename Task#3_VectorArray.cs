using System;
using System.Text;

namespace Epam.Tasks.VectorArray
{
	class VectorArray
	{		
		public VectorArray()
		{
			_array = new int[0];
			Length = 0;
		}
		public VectorArray(int length)
		{
			if (length >= 0)
			{
				_array = new int[length];
			    Length = length;
			}
			else
			{
				_array = new int[0];
			    Length = 0;
			}
		}
		
		public int Length { get; private set; }
		
		public int this[int i]
		{
			get
			{
				if (CorrespondToArrayLength(i))
				{
					return _array[i];
				}
				else
				{
					throw new IndexOutOfRangeException();
				}
			}
			set
			{
				if (CorrespondToArrayLength(i))
				{
					_array[i] = value;
				}
				else
				{
					throw new IndexOutOfRangeException();
				}
			}
		}	
		
		public override bool Equals(object obj)
		{
			VectorArray other = obj as VectorArray;
			
			if (obj == null || Length != other.Length)
			{
				return false;
			}
			else
			{				
				for (var i = 0; i < Length; i++)
				{
					if (this[i] != other[i])
					{
						return false;
					}
				}
				
				return true;
			}
		}		
		public override int GetHashCode()
		{
			int hash = 7;
			
			for (var i = 0; i < Length; i++)
			{
				hash = hash * 13 + this[i];
			}
			
			return hash;
		}
		public override string ToString()
		{
			StringBuilder str = new StringBuilder("{", Length * 2);
			
			for (var i = 0; i < Length; i++)
			{
				str.Append(this[i]);
				str.Append(" ");
			}
			
			str.Append("}");
			return str.ToString();
		}
		public void Add(VectorArray other)
		{
			if (Length == other.Length)
			{
				for (var i = 0; i < Length; i++)
				{
					this[i] += other[i];
				}
			}
			else
			{
				throw new IndexOutOfRangeException();
			}
		}
		public void Subtract(VectorArray other)
		{
			if (Length == other.Length)
			{
				for (var i = 0; i < Length; i++)
				{
					this[i] -= other[i];
				}
			}
			else
			{
				throw new IndexOutOfRangeException();
			}
		}
		public void Multiply(int mult)
		{	
			for (var i = 0; i < Length; i++)
			{
				this[i] *= mult;
			}
		}		
		public VectorArray GetSubVector(int firstIndex, int length)
		{
			VectorArray other = new VectorArray(length);
			
			for (var i = 0; i < other.Length; i++)
			{
				other[i] = this[i + firstIndex];
			}
			
			return other;
		}
		public VectorArray GetSubVector(int firstIndex)
		{
			VectorArray other = new VectorArray(Length - firstIndex);
			
			for (var i = 0; i < other.Length; i++)
			{
				other[i] = this[i + firstIndex];
			}
			
			return other;
		}
		public static VectorArray Add(VectorArray first, VectorArray second)
		{
			if (first.Length == second.Length)
			{
				VectorArray sum = new VectorArray(first.Length);
				
				for (var i = 0; i < first.Length; i++)
				{
					sum[i] = first[i] + second[i];
				}
				
				return sum;
			}
			else
			{
				throw new IndexOutOfRangeException();
			}
		}
		public static VectorArray Subtract(VectorArray first, VectorArray second)
		{
			if (first.Length == second.Length)
			{
				VectorArray sum = new VectorArray(first.Length);
				
				for (var i = 0; i < first.Length; i++)
				{
					sum[i] = first[i] - second[i];
				}
				
				return sum;
			}
			else
			{
				throw new IndexOutOfRangeException();
			}
		}
		public static VectorArray Multiply(VectorArray array, int mult)
		{
			VectorArray product = new VectorArray(array.Length);
			
			for (var i = 0; i < product.Length; i++)
			{
				product[i] = array[i] * mult;
			}
			
			return product;
		}
		public static VectorArray Multiply(int mult, VectorArray array)
		{
			VectorArray product = new VectorArray(array.Length);
			
			for (var i = 0; i < product.Length; i++)
			{
				product[i] = array[i] * mult;
			}
			
			return product;
		}
		
		public static bool operator ==(VectorArray first, VectorArray second)
		{
			return first.Equals(second);
		}
		public static bool operator !=(VectorArray first, VectorArray second)
		{
			return !first.Equals(second);
		}
		public static VectorArray operator +(VectorArray first, VectorArray second)
		{
			return Add(first, second);
		}
		public static VectorArray operator -(VectorArray first, VectorArray second)
		{
			return Subtract(first, second);
		}
		public static VectorArray operator *(VectorArray array, int mult)
		{
			return VectorArray.Multiply(array, mult);
		}
		public static VectorArray operator *(int mult, VectorArray array)
		{
			return VectorArray.Multiply(mult, array);
		}
		
		private int[] _array;
		
		private bool CorrespondToArrayLength(int index)
		{
			bool positive = (index >= 0);
			bool lessThanLength = (index < Length);
			
			return (positive && lessThanLength);
		}
	}
	
	class RepresentingVectorArrays
	{
		static void Main()
		{
			// Creating and initializing vector-arrays
			var v1 = new VectorArray(5);
			for (var i = 0; i < v1.Length; i++)
			{
				v1[i] = i;
			}
			Console.WriteLine("v1 = {0}", v1);
			
			var v2 = new VectorArray(5);
			for (var i = 0; i< v2.Length; i++)
			{
				v2[v2.Length - i - 1] = i;
			}
			Console.WriteLine("v2 = {0}", v2);
			
			var v3 = new VectorArray(5);
			for (var i = 0; i < v3.Length; i++)
			{
				v3[i] = i;
			}
			Console.WriteLine("v3 = {0}", v3);
			Console.WriteLine();
			
			// Getting vector`s element by index
			int firstElement = v1[1];
			Console.WriteLine("The first element of vector-array {0} is {1}", v1, firstElement);
			
			int fifthElement = v1[4];
			Console.WriteLine("The fifth element of vector-array {0} is {1}", v1, fifthElement);
			Console.WriteLine();
			
			// Getting subvector
			VectorArray subVector = v1.GetSubVector(2, 3);
			Console.WriteLine("New vector-array formed from {0} equals {1}", v1, subVector);
			
			subVector = v1.GetSubVector(2);
			Console.WriteLine("New vector-array formed from {0} equals {1}", v1, subVector);
			Console.WriteLine();
			
			// Adding and subtraction vector-arrays
			VectorArray v1PlusV2 = VectorArray.Add(v1, v2);
			Console.WriteLine("The sum of {0} and {1} equals {2}", v1, v2, v1PlusV2);
			VectorArray v1MinusV2 = VectorArray.Subtract(v1, v2);
			Console.WriteLine("The subtract of {0} and {1} equals {2}", v1, v2, v1MinusV2);
			
			v1PlusV2 = v1 + v2;
			Console.WriteLine("The sum of {0} and {1} equals {2}", v1, v2, v1PlusV2);
			v1MinusV2 = v1 - v2;
			Console.WriteLine("The subtract of {0} and {1} equals {2}", v1, v2, v1MinusV2);
			
			Console.Write("The sum of {0} and {1} equals ", v1, v2);
			v1.Add(v2);
			Console.WriteLine(v1.ToString());
			
			Console.Write("The subtract of {0} and {1} equals: ", v1, v2);
			v1.Subtract(v2);
			Console.WriteLine(v1);
			Console.WriteLine();
			
			// Multiplying vectors by a scalar  
			VectorArray v2Mult2 = VectorArray.Multiply(v2, 2);
			Console.WriteLine("{0} multiplied by 2 equals {1}", v2, v2Mult2);
			
			v2Mult2 = v2 * 2;
			Console.WriteLine("{0} multiplied by 2 equals {1}", v2, v2Mult2);
			
			Console.Write("{0} multiplied by 2 equals: ", v2);
			v2.Multiply(2);
			Console.WriteLine(v2.ToString());
			
			// Comparing vectors
			bool v1EqualsV2 = v1.Equals(v2);
			Console.WriteLine("{0} equals {1}: {2}", v1, v2, v1EqualsV2);
			
			bool v1EqualsV3 = (v1 == v3);
			Console.WriteLine("{0} equals {1}: {2}", v1, v3, v1EqualsV3);
			Console.WriteLine();
		}
	}
}